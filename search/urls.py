__author__ = 'Thorne'

from django.urls import path
from . import views
from django.views.generic import TemplateView

urlpatterns = [
    # path('', TemplateView.as_view(template_name='index.html'), name='index')
    path('', views.IndexView.as_view(), name='index'),
    path('suggest', views.SearchSuggest.as_view(), name='suggest'),
    path('search', views.SearchView.as_view(), name='search')
]